import 'package:flutter/material.dart';
import 'package:flutter_nfc_kit/flutter_nfc_kit.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'NFC Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
    String handle = "";
    String data = "";
    late var isAvailable;
    bool isEnabled = true;


    void startSession() async{
      isAvailable = await FlutterNfcKit.nfcAvailability;
      if(isAvailable == NFCAvailability.available){
        setState(() {
          isEnabled = false;
        });
        var tag = await FlutterNfcKit.poll(); 
          setState(() {
            data = tag.toJson().toString();
            isEnabled = true;
        } );
        FlutterNfcKit.finish();
      }else{
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("NFC not available")));
      }
      }

      void stopSession(){
        setState(() {
          isEnabled = true;
        });
        FlutterNfcKit.finish();
      }
 
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center( 
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(onPressed: isEnabled ? startSession : null, child: Text("Start searching"),),
            ElevatedButton(onPressed: isEnabled ? null : stopSession, child:  Text("Stop searching")),
            Text("Data: " + data),
          ],
        ),
      ),
      
    );
  }
}
